
.PHONY: docs build

docs:
	python3 -m mkdocs serve

s: docs

build:
	python3 -m mkdocs build

b: build
