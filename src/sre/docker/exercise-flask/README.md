
### Python flask app

- python flask 🐳 ✅
- (db) Redis 🐳

Investigate and try to digest the directory with the files, try to understand what the app.py does, and how the Dockerfile is built.

### Run?

```bash
cd src/sre/docker/exercise-flask

docker build -t python-flask .

docker network create flask-redis

docker run -d --network=flask-redis --name redis redis

docker run -p 8000:5000 --network=flask-redis -it --env "FOO=docker run" python-flask

```

### Exercise [Docker-compose](https://docs.docker.com/compose/compose-file/compose-file-v3/)

Write a docker-compose.yaml file that:

- has a service named `redis`
- has a service called `web`
- builds the app with `build: .`
- exposes the `web` app on port 8000
- injects and receives an environment variable `MESSAGE`

- run your stack with `docker-compose up --build`
