locals {
  container_count = 3
}

resource "docker_image" "nginx" {
  name         = var.image_name
  keep_locally = false
}

resource "docker_container" "nginx" {
  count = local.container_count
  image = docker_image.nginx.name
  name  = "terraform-tutorial-${count.index}"

  # ports {
  #   internal = 80
  #   external = 8080
  # }

  labels {
    label = "environment"
    value = "development"
  }
}



output "container_id" {
  description = "ID of the Docker container"
  value       = docker_container.nginx[*].id
}

output "image_id" {
  description = "ID of the Docker image"
  value       = docker_image.nginx.id
}
